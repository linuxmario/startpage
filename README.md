# startpage

## Français

Une page d'accueil simple et contenant l'essentiel de ce qu'il me faut au quotidien.
Elle est hébergée sur mon serveur local sous OpenBSD et me sert à ne pas aller directement sur le web pour chercher quelque choe et à changer mes habitudes.

 * Inspiré par le projet suivant : [Startpage](https://github.com/dylanaraps/startpage)
 * Licence : MIT

## English

A simple start page which have all of the essential tools I use everyday.
It is hosted on my local OpenBSD server and its main goal is to give me what I need without doing a web search (in order to change my habits).

 * Inspired by the following project : [Startpage](https://github.com/dylanaraps/startpage)
 * License : MIT

## Exemple / Example

![Capture d'écran de ma page d'accueil / Screenshot of my startup page](capture.png)
